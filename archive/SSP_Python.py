import random

P_Leben = 3
G_Leben = 3
Spiele = 0

def gegner_attack():
    possibilities = [0, 1, 2]
    return_attack = random.choice(possibilities)
    return return_attack

def enemy_dead():
    global Spiele
    if G_Leben == 0:
        print("Du hast gewonnen, congrats!")
        Spiele = None

def player_dead():
    global Spiele
    if P_Leben == 0:
        print("Du bist gestorben!")
        Spiele = None

while True:
    if Spiele is None:
        print("Das Spiel wird hiermit beendet!")
        break
    Eingabe = int(input("Bitte geben Sie eine Fähigkeit ein (0 - Schere, 1 - Stein, 2 - Papier): "))
    gegner_attack_result = gegner_attack()
    print("Der Gegner hat folgenden Angriff:", gegner_attack_result)

    if Eingabe == gegner_attack_result:
        print("Niemand verliert ein Leben :)")

    elif Eingabe == 0:
        if gegner_attack_result == 1:
            P_Leben -= 1
            player_dead()
            print("Du hast 1 Leben verloren:", P_Leben)

        elif gegner_attack_result == 2:
            G_Leben -= 1
            enemy_dead()
            print("Der Gegner hat 1 Leben verloren:", G_Leben)

    elif Eingabe == 1:
        if gegner_attack_result == 0:
            G_Leben -= 1
            enemy_dead()
            print("Der Gegner hat 1 Leben verloren:", G_Leben)

        elif gegner_attack_result == 2:
            P_Leben -= 1
            player_dead()
            print("Der Spieler hat ein Leben verloren:", P_Leben)

    elif Eingabe == 2:
        if gegner_attack_result == 0:
            P_Leben -= 1
            player_dead()
            print("Der Spieler hat ein Leben verloren:", P_Leben)

        elif gegner_attack_result == 1:
            G_Leben -= 1
            enemy_dead()
            print("Der Gegner hat ein Leben verloren:", G_Leben)
