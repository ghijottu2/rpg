-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 17. Apr 2024 um 13:56
-- Server-Version: 10.4.28-MariaDB
-- PHP-Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `projekt`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `faehigkeiten`
--

CREATE TABLE `faehigkeiten` (
  `F_ID` int(11) NOT NULL,
  `F_Name` varchar(20) DEFAULT NULL,
  `F_Desc` varchar(50) DEFAULT NULL,
  `F_Standard` tinyint(1) DEFAULT NULL,
  `K_ID` int(11) DEFAULT NULL,
  `G_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `faehigkeiten`
--

INSERT INTO `faehigkeiten` (`F_ID`, `F_Name`, `F_Desc`, `F_Standard`, `K_ID`, `G_ID`) VALUES
(0, 'Einfacher Schlag', 'Erinnert mich an meine Kindheit', 1, 0, 0),
(1, 'Explosionsschlag', 'Erinnert mich an meine Kindheit', 1, 0, 0),
(2, 'Spin', 'Bis dir schlecht wird', 0, 0, 0),
(3, 'Jump and Hit', 'Wie Jump n Run aber mit Schlägen', 0, 0, 1),
(4, 'Feuerschlag', 'Erinnert mich erst recht an meine Kindheit', 0, 0, 2),
(5, 'Einfacher Stich', 'POV: du bist in London', 1, 1, 1),
(6, 'Frost', 'Wie die Beziehung zu meinen Eltern', 1, 1, 1),
(7, 'Verstärkter Stich', 'POV: du bist in London - 2.0', 0, 1, 1),
(8, 'Eisschlag', 'GET THESE HANDS, oder so', 0, 1, 0),
(9, 'Splash', 'Macht den Gegner nass', 0, 1, 2),
(10, 'Einfacher Schnitt', 'Einfach, aber schmerzhaft', 1, 2, 2),
(11, 'Ground Hit', 'Manchmal hhilft es einfach zuzuschlagen', 1, 2, 2),
(12, 'Schnitt 2x', 'Doppelt hält besser', 0, 2, 2),
(13, 'Messerwurf', 'Musst du aber selbst zurückholen', 0, 2, 0),
(14, 'Agressiver Schnitt', 'Hier werden Gefühle verarbeitet', 0, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gegner`
--

CREATE TABLE `gegner` (
  `G_ID` int(11) NOT NULL,
  `G_Name` varchar(50) DEFAULT NULL,
  `G_Desc` varchar(50) DEFAULT NULL,
  `G_Leben` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `gegner`
--

INSERT INTO `gegner` (`G_ID`, `G_Name`, `G_Desc`, `G_Leben`) VALUES
(0, 'Skeleton', 'Gefährlicher als die Minecraft Version', 2),
(1, 'Nightborne', 'Feind der Natur und von euch', 3),
(2, 'Boss', 'Hasst dich', 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `klassen`
--

CREATE TABLE `klassen` (
  `K_ID` int(11) NOT NULL,
  `K_Name` varchar(20) DEFAULT NULL,
  `K_Desc` varchar(50) DEFAULT NULL,
  `K_Waffe` varchar(50) DEFAULT NULL,
  `K_Leben` int(3) DEFAULT NULL,
  `K_Strength` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `klassen`
--

INSERT INTO `klassen` (`K_ID`, `K_Name`, `K_Desc`, `K_Waffe`, `K_Leben`, `K_Strength`) VALUES
(0, 'Schwertkämpfer', 'Ritter aber in cool!', 'Feuerschwert', 4, 0),
(1, 'Priester', 'Nasser als Mom!', 'Gehstock', 3, 1),
(2, 'Assassine', 'Kein Wickinger!!!', 'Dolch', 3, 2);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `faehigkeiten`
--
ALTER TABLE `faehigkeiten`
  ADD PRIMARY KEY (`F_ID`),
  ADD UNIQUE KEY `F_ID` (`F_ID`),
  ADD KEY `Klassen.K_ID` (`K_ID`),
  ADD KEY `Gegner.G_ID` (`G_ID`);

--
-- Indizes für die Tabelle `gegner`
--
ALTER TABLE `gegner`
  ADD PRIMARY KEY (`G_ID`),
  ADD UNIQUE KEY `G_ID` (`G_ID`);

--
-- Indizes für die Tabelle `klassen`
--
ALTER TABLE `klassen`
  ADD PRIMARY KEY (`K_ID`),
  ADD UNIQUE KEY `K_ID` (`K_ID`),
  ADD KEY `GID` (`K_Strength`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `faehigkeiten`
--
ALTER TABLE `faehigkeiten`
  ADD CONSTRAINT `Gegner.G_ID` FOREIGN KEY (`G_ID`) REFERENCES `gegner` (`G_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Klassen.K_ID` FOREIGN KEY (`K_ID`) REFERENCES `klassen` (`K_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `klassen`
--
ALTER TABLE `klassen`
  ADD CONSTRAINT `GID` FOREIGN KEY (`K_Strength`) REFERENCES `gegner` (`G_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
