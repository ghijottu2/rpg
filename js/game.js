// Needed for transfering variables from script.js
let selectedKlasse = JSON.parse(localStorage.getItem("selectedClass")); // hiermit laden wir Variablen aus dem Local Storage
let S_Faehigkeit1 = JSON.parse(localStorage.getItem("S_Faehigkeit1")); // diese wurden zuvor in script.js in den Local Storage übertragen
let S_Faehigkeit2 = JSON.parse(localStorage.getItem("S_Faehigkeit2")); // wir finden diese über deren IDs -> z.B. selectedClass
let Faehigkeit = JSON.parse(localStorage.getItem("Faehigkeit"));
var LoggingG = JSON.parse(localStorage.getItem("LoggingG"));



// Misc variables for the game
let Gegner = [];
let GameID = 0;
let currentEnemy = 0;
let Spiele = true;
let lastrandom = null;
let result = [];

// Auslesen aller Gegner
for (let i = 0; i < LoggingG.length; i++) {
  Gegner[i] = JSON.parse(localStorage.getItem(LoggingG[i]));
}

// Variables for the info box
let enemy_dead = "Gegner ist gestorben!";
let you_dead = "Du bist gestorben!";
let tie = "Kein Treffer, beide Attacken waren wirkungslos!";
let damage_you = "Diese Attacke war schwach, du bekommst Schaden!";
let damage_enemy = "Du hast einen Schadenspunkt ausgeteilt!";
let damage_enemy2 = "Kritischer Treffer! Gegner bekommt 2 Schadenspunkte";
let empty = "";
let enemy = "";

// Variables for music
let boss_theme = new Audio("./media/sounds/boss.mp3");
let end = new Audio("./media/sounds/game-over.mp3");

// Variables for sounds
let hitmarker = new Audio("./media/sounds/hitmarker.mp3");
let heartbeat = new Audio("./media/sounds/heartbeat.mp3");
let metalpipe = new Audio("./media/sounds/metalpipe.mp3");

// Variables for enemies
var slideSourceOut = document.getElementById("slideSourceOut");
var slideSourceIn = document.getElementById("slideSourceIn");
var BloodMoonTower = document.getElementById("BloodMoonTower");
var Skeleton_idle = document.getElementById("Skeleton_idle");
var Skeleton_walk = document.getElementById("Skeleton_walk");
var Skeleton_death = document.getElementById("Skeleton_death");
var Skeleton_attack = document.getElementById("Skeleton_attack");
var Nightborne_attack = document.getElementById("Nightborne_attack");
var Nightborne_idle = document.getElementById("Nightborne_idle");
var Nightborne_death = document.getElementById("Nightborne_death");
var Boss_idle = document.getElementById("Boss_idle");
var Boss_attack = document.getElementById("Boss_attack");
var Boss_death = document.getElementById("Boss_death");


// ???
document.getElementById("S_Faehigkeit1").textContent = S_Faehigkeit1.F_Name;
document.getElementById("S_Faehigkeit2").textContent = S_Faehigkeit2.F_Name;
document.getElementById("Faehigkeit").textContent = Faehigkeit.F_Name;


if (selectedKlasse.K_Name == "Schwertkämpfer") {
  character = "schwertkaempfer";
  character_take_hit_duration = 600;
}

if (selectedKlasse.K_Name == "Priester") {
  character = "priester";
  character_take_hit_duration = 700;
}

if (selectedKlasse.K_Name == "Assassine") {
  character = "assassine";
  character_take_hit_duration = 600;
}

// Use variables to identify current enemy and make the enemy animations modular
if (Gegner[currentEnemy].G_Name == "Skeleton") {
  enemy = "Skeleton";
}

if (Gegner[currentEnemy].G_Name == "Nightborne") {
  enemy = "Nightborne";
  enemy_take_hit_duration = "350";
}

if (Gegner[currentEnemy].G_Name == "Boss") {
  enemy = "Boss";
  enemy_take_hit_duration = "500";
}

// Load selected ability into variable
if (
  Faehigkeit.F_Name == "Spin" || Faehigkeit.F_Name == "Verstärkter Stich" || Faehigkeit.F_Name == "Schnitt 2x") {
    attack_selected = "attack1";
}

if (
  Faehigkeit.F_Name == "Jump and Hit" || Faehigkeit.F_Name == "Eisschlag" || Faehigkeit.F_Name == "Messerwurf") {
    attack_selected = "attack2";
}

if (
  Faehigkeit.F_Name == "Feuerschlag" || Faehigkeit.F_Name == "Splash" || Faehigkeit.F_Name == "Agressiver Schnitt") {
    attack_selected = "attack3";
}

if (S_Faehigkeit1.F_Name == "Einfacher Schlag" || S_Faehigkeit1.F_Name == "Einfacher Stich" || S_Faehigkeit1.F_Name == "Einfacher Schnitt") {
  attack_SF1_normal = "attack4";
}

if (S_Faehigkeit2.F_Name == "Explosionsschlag" || S_Faehigkeit2.F_Name == "Frost" || S_Faehigkeit2.F_Name == "Ground Hit") {
  attack_SF2_special = "attack_special";
}

// pauseFunction needed for various purposes
function pauseFunction(milliseconds, callback) {
  setTimeout(callback, milliseconds);
}

// Options (rock, paper, siccsor) get assigned to classes
function randomizeAbility() {
  var possibilities = [0, 1, 2];

  for (let i = 0; i <= 2; i++) {
    var random = Math.floor(Math.random() * possibilities.length);
    result.push(possibilities[random]);
    possibilities.splice(random, 1);
    assignAbility(i);
  }
  console.log("Abilities erfolgreich zugeordnet");
  return true;
}

randomizeAbility();

function assignAbility(number) {
  if (number == 0) {
    if (result[0] == 0) {
      S_Faehigkeit1.classification = "Schere";
    } else if (result[0] == 1) {
      S_Faehigkeit1.classification = "Stein";
    } else if (result[0] == 2) {
      S_Faehigkeit1.classification = "Papier";
    }
  } else if (number == 1) {
    if (result[1] == 0) {
      S_Faehigkeit2.classification = "Schere";
    } else if (result[1] == 1) {
      S_Faehigkeit2.classification = "Stein";
    } else if (result[1] == 2) {
      S_Faehigkeit2.classification = "Papier";
    }
  } else if (number == 2) {
    if (result[2] == 0) {
      Faehigkeit.classification = "Schere";
    } else if (result[2] == 1) {
      Faehigkeit.classification = "Stein";
    } else if (result[2] == 2) {
      Faehigkeit.classification = "Papier";
    }
  }
  return true;
}

function info_tie() {
  console.log("Diese Attacke ist völlig wirkungslos, du Opfer!");
  document.getElementById("info-content").innerHTML = tie; // in die Infobox wird der Text aus der Variable tie geladen
  document.getElementById("info-container").style.visibility = "visible"; // die Infobox wird angezeigt
  pauseFunction(2000, function () {
    document.getElementById("info-container").style.visibility = "hidden"; // nach 2s wird die Infobox wieder unsichtbar
    document.getElementById("info-content").innerHTML = empty; // Inhalt der Infobox wird geleert
    document.getElementById(`button${button}`).style.backgroundColor = "#ffffff"; // der Button, der ausgewählt wurde, bekommt wieder eine weiße Hintergrundfarbe
    console.log("Ausgeführt in enemydead tie");
    label_pointer_on(); // Alle Buttons sind wieder auswählbar
  });
}

function character_damage() {
  document.getElementById(`${Gegner[currentEnemy].G_Name}_idle`).style.visibility = "hidden"; // Idle-Animation des Gegners wird versteckt
  document.getElementById(`${Gegner[currentEnemy].G_Name}_attack`).style.visibility = "visible"; // Kampf-Animation des Gegners wird angezeigt
  pauseFunction(2000, function () {
  document.getElementById(`${Gegner[currentEnemy].G_Name}_attack`).style.visibility = "hidden"; // Kampf-Animation des Gegners wird versteckt
  document.getElementById(`${Gegner[currentEnemy].G_Name}_idle`).style.visibility = "visible"; // Idle-Animation des Gegners wird wieder angezeigt
});
  hitmarker.play();  // Hitmarker-Sound wird abgespielt
  selectedKlasse.K_Leben = selectedKlasse.K_Leben - 1; // dem Charakter wird ein Leben abgezogen
  document.getElementById("hearts-character-number").innerHTML = selectedKlasse.K_Leben; // die neue Lebensanzahl des Charakters wird angezeigt
  console.log("Der Angriff des Gegners war effektiv. Du verlierst ein Leben! Jetzt nur noch: " + selectedKlasse.K_Leben);

  if (selectedKlasse.K_Leben == 1) {
    document.getElementById("hearts-character").style.backgroundColor = "#e86969"; // Hat der Charakter nur noch ein Leben, wird die Hintergrundfarbe der Lebensanzeige rot
    heartbeat.loop = true; // zusätzlich wird ein Herzklopfen-Sound abgespielt
    heartbeat.play();
  } 
  else { // Wenn die Leben des Charakters ungleich 1 sind:
    document.getElementById(`${character}_idle`).style.visibility = "hidden"; // Idle-Animation des Charakters wird versteckt
    document.getElementById(`${character}_take_hit`).style.visibility = "visible"; // Schadens-Animation des Charakters wird angezeigt
    pauseFunction(`${character_take_hit_duration}`, function () { // für die spezifizierte Anzahl an ms wird die Schadensanimation angezeigt
      document.getElementById(`${character}_take_hit`).style.visibility = "hidden"; // Schadens-Animation des Charakters wird versteckt
      if (selectedKlasse.K_Leben > 0){
    document.getElementById(`${character}_idle`).style.visibility = "visible"; // Wenn der Gegner nicht tot ist, wird die Idle-Animation wieder angezeigt
     }
    });
    character_dead(); // Überprüfung, ob der Charakter tot ist
    document.getElementById("info-content").innerHTML = damage_you; // Anzeige der Infobox, dass der Charakter Schaden genommen hat, für genauere Beschreibung siehe info_tie()
    document.getElementById("info-container").style.visibility = "visible";
    pauseFunction(1000, function () {
      document.getElementById("info-container").style.visibility = "hidden";
      document.getElementById("info-content").innerHTML = empty;
    });
  }
  pauseFunction(2000, function () {
  label_pointer_on();
  document.getElementById(`button${button}`).style.backgroundColor = "#ffffff";
  console.log("Ausgeführt in Playerdamage");
  })
}

function enemy_attack() {
  possibilities = ["Schere", "Stein", "Papier"];
  returnAttack = possibilities[Math.floor(Math.random() * possibilities.length)];

  while (returnAttack == lastrandom) {
    returnAttack = possibilities[Math.floor(Math.random() * possibilities.length)];
    console.log("Eine neue Zahl musste generiert werden!");
  }
  lastrandom = returnAttack;
  console.log("Gegner hat: " + returnAttack);
  return returnAttack;
}

function label_pointer_off() { // Alle Buttons sind nicht mehr anklickbar
  document.getElementById("label1").style.pointerEvents = "none";
  document.getElementById("label2").style.pointerEvents = "none";
  document.getElementById("label3").style.pointerEvents = "none";
}

function label_pointer_on() { // Alle Buttons sind wieder anklickbar
  document.getElementById("label1").style.pointerEvents = "all";
  document.getElementById("label2").style.pointerEvents = "all";
  document.getElementById("label3").style.pointerEvents = "all";
}

function enemy_damage_animation(){
  if (Gegner[currentEnemy].G_Name == "Nightborne" || Gegner[currentEnemy].G_Name == "Boss"){ // Abfrage welche Gegner zu sehen sind
    document.getElementById(`${Gegner[currentEnemy].G_Name}_idle`).style.visibility = "hidden"; // Idle-Animation des Gegners wird versteckt
    document.getElementById(`${Gegner[currentEnemy].G_Name}_take_hit`).style.visibility = "visible";  // Schadens-Animation des Gegners wird angezeigt
    pauseFunction(`${enemy_take_hit_duration}`, function () { // nach spezifizierter Anzahl von ms:
      document.getElementById(`${Gegner[currentEnemy].G_Name}_take_hit`).style.visibility = "hidden"; // Schadens-Animation des Gegners wird versteckt
      document.getElementById(`${Gegner[currentEnemy].G_Name}_idle`).style.visibility = "visible"; // Idle-Animation des Gegners wird angezeigt
    });
}
}

function enemy_damage(selected_Faehigkeit) { // für Vergleich und genauere Beschreibungen siehe character_damage(), neue Inhalte wurden kommentiert
  if (searchStrengths(selectedKlasse.K_Strength, selected_Faehigkeit, Gegner[currentEnemy].G_ID) == true) { // Abfrage ob aktuelle Fähigkeit eine Stärke ist
    hitmarker.play();
    enemy_damage_animation(); // für Beschreibung der Funktion siehe enemy_damage_animation()
    Gegner[currentEnemy].G_Leben = Gegner[currentEnemy].G_Leben - 2;
    document.getElementById("hearts-enemy-number").innerHTML = Gegner[currentEnemy].G_Leben;
    console.log("Dein Angriff macht besonders viel Schaden, weil dieser Gegner schwach ist! Neues Leben vom Gegner: " + Gegner[currentEnemy].G_Leben);
    if(Gegner[currentEnemy].G_Leben > 0){
      pauseFunction(2000, function () {
        document.getElementById(`button${button}`).style.backgroundColor = "#ffffff";
      console.log("Ausgeführt in enemydamage -2");
      label_pointer_on();
    }
  )};
    enemy_is_dead();
    document.getElementById("info-content").innerHTML = damage_enemy2;
    document.getElementById("info-container").style.visibility = "visible";
    pauseFunction(2000, function () {
      document.getElementById("info-container").style.visibility = "hidden";
      document.getElementById("info-content").innerHTML = empty;
    });
  } 
  else { // (Wenn aktueller Angriff keine Stärke ist und damit nur 1 Schadenpunkt ausgeteilt wird), für Vergleich siehe oben
    hitmarker.play();  // Hitmarker-Sound wird abgespielt
    enemy_damage_animation();
    Gegner[currentEnemy].G_Leben = Gegner[currentEnemy].G_Leben - 1;
    document.getElementById("hearts-enemy-number").innerHTML = Gegner[currentEnemy].G_Leben;
    console.log("Du hast Schaden gemacht! Neues Leben vom Gegner: " + Gegner[currentEnemy].G_Leben);
    if(Gegner[currentEnemy].G_Leben > 0){
      pauseFunction(2000, function () {
      document.getElementById(`button${button}`).style.backgroundColor = "#ffffff";
      console.log("Ausgeführt in enemydamage -1");
      label_pointer_on();
      });
    }
    enemy_is_dead();
    document.getElementById("info-content").innerHTML = damage_enemy;
    document.getElementById("info-container").style.visibility = "visible";
    pauseFunction(2000, function () {
      document.getElementById("info-container").style.visibility = "hidden";
      document.getElementById("info-content").innerHTML = empty;
    });
  }
}

function enemy_is_dead() {
  if (Gegner[currentEnemy].G_Leben <= 0) { // Abfrage ob Gegner 0 oder weniger Leben (1-2 = -1) hat 
    console.log("Dieser Gegner ist tot, aber es kommt ein neuer! (Manuel Neuer)");
    if (Gegner[currentEnemy].G_Name == "Nightborne" || Gegner[currentEnemy].G_Name == "Boss"){ // Bugfix, weil Schadens-Animationen von Gegner 2 und 3 nicht verschwinden
      document.getElementById("Nightborne_take_hit").style.visibility = "hidden";
      document.getElementById("Boss_take_hit").style.visibility = "hidden";
    }

    currentEnemy = currentEnemy + 1; // Wenn Gegner besiegt wurde, wird Gegneranzahl hochgezählt
    document.getElementById("info-content").innerHTML = enemy_dead; // für Beschreibung der Infobox siehe info_tie()
    document.getElementById("info-container").style.visibility = "visible";
    pauseFunction(2000, function () {
      document.getElementById("info-container").style.visibility = "hidden";
      document.getElementById("info-content").innerHTML = empty;
    });
    document.getElementById(`${enemy}_idle`).style.visibility = "hidden"; // Idle-Animation des Gegners wird versteckt
    document.getElementById(`${enemy}_death`).style.visibility = "visible"; // Todesanimation des Gegners wird angezeigt
    document.getElementById("hearts-enemy").style.visibility = "hidden"; // Gegnerherzen werden versteckt
    document.getElementById("button1").style.backgroundColor = "#adafbc"; // Buttons werden ausgegraut
    document.getElementById("button2").style.backgroundColor = "#adafbc";
    document.getElementById("button3").style.backgroundColor = "#adafbc";
    label_pointer_off(); // Buttons sind nicht mehr anklickbar

    pauseFunction(2000, function () {
      if (currentEnemy == 1) { // Wenn Gegner 1 gestorben ist, jetzt Gegner 2 geladen wird:
        enemy = "Nightborne";
        enemy_take_hit_duration = "350"; // spezifikation der Schadensanimationsdauer

        pauseFunction(2000, function () {
        Skeleton_death.classList.toggle("dark"); // Gegner 1 verschwindet
        });  
        pauseFunction(1000, function () {
          Nightborne_idle.classList.toggle("light"); // Gegner 2 taucht auf
          document.getElementById("hearts-enemy-number").innerHTML = Gegner[currentEnemy].G_Leben; // Neue Lebensanzahl des Gegners wird angezeit
          document.getElementById("hearts-enemy").style.visibility = "visible";
          pauseFunction(2000, function () {
            document.getElementById("button1").style.backgroundColor = "#ffffff"; // Buttons sind nicht mehr ausgegraut
            document.getElementById("button2").style.backgroundColor = "#ffffff";
            document.getElementById("button3").style.backgroundColor = "#ffffff";
            console.log("Ausgeführt in enemydead char1");
            label_pointer_on(); // Buttons sind wieder anklickbar
          });
        });
      }

      else if (currentEnemy == 2) { // für Vergleich siehe if (currentEnemy == 1)
        enemy = "Boss";
        enemy_take_hit_duration = "500";
        pauseFunction(2000, function () {
        Nightborne_death.classList.toggle("dark");
        });  
        document.getElementById("Nightborne_idle").style.visibility = "hidden";
        document.getElementById("Nightborne_death").style.visibility = "visible";
        pauseFunction(3000, function () {
          Boss_idle.classList.toggle("light");
          document.getElementById("hearts-enemy-number").innerHTML = Gegner[currentEnemy].G_Leben;
          document.getElementById("hearts-enemy").style.visibility = "visible";
          pauseFunction(2000, function () {
            document.getElementById("button1").style.backgroundColor = "#ffffff";
            document.getElementById("button2").style.backgroundColor = "#ffffff";
            document.getElementById("button3").style.backgroundColor = "#ffffff";
            console.log("Ausgeführt in enemydead char2");
            label_pointer_on();
          });
        });
      } 
      
      else if (currentEnemy == 3) { // für Vergleich siehe if (currentEnemy == 1)
        // document.getElementById("Boss_take_hit").style.visibility = "hidden";
        Boss_idle.classList.toggle("dark");
        document.getElementById("Boss_idle").style.visibility = "hidden";
        document.getElementById("Boss_death").style.visibility = "visible";
        $(boss_theme).animate({ volume: 0 }, 3000); // Wenn 3. Gegner gestorben ist, Fade-Out Hintergrundmusik
        end.play; // End-Musik startet
        pauseFunction(4000, function () {
          alert("You are now allowed to touch some grass 😌");
          slideSourceIn.classList.toggle("black"); // Fade-Out für Bildschirm
          pauseFunction(3000, function () {
            window.location.replace("https://www.youtube.com/watch?v=lc17rmj1K90"); // Weiterleitung zu YouTube-Video
          });
        });
      }
    });
  }

  if (currentEnemy >= LoggingG.length) {
    Spiele = false;
  }
}


function searchStrengths(K_Strength, F_Strength, EnemyID) {
  if (K_Strength == Gegner[EnemyID].G_ID || F_Strength == Gegner[EnemyID].G_ID) {
    return true;
  } else {
    console.log("Leider griff die Bedingung nicht! ");
    return false;
  }
}

function character_dead() { // für Vergleich siehe if enemy_is_dead()
  if (selectedKlasse.K_Leben <= 0) {
    document.getElementById(`${character}_idle`).style.visibility = "hidden";
    document.getElementById(`${character}_take_hit`).style.visibility = "hidden";
    document.getElementById(`${character}_death`).style.visibility = "visible";
    console.log("Du bist gestorben!");
    heartbeat.pause();
    metalpipe.play();
    document.getElementById("info-content").innerHTML = you_dead;
    document.getElementById("info-container").style.visibility = "visible";
    pauseFunction(2000, function () {
      document.getElementById("info-container").style.visibility = "hidden";
      document.getElementById("info-content").innerHTML = empty;
    });
    playDeathScreen(); // Für Beschreibung siehe Funktion playDeathScreen()
    Spiele = false;
    return true;
  }
}

function gameLogic() {
  Gegnerangriff = enemy_attack();

  if (Spiele == true) {
    if (S_Faehigkeit1.selected == true) {
      if (S_Faehigkeit1.classification == Gegnerangriff) {
        info_tie();
      } 
      else if (S_Faehigkeit1.classification == "Schere") {
        if (Gegnerangriff == "Stein") {
          character_damage();
        } 
        else if (Gegnerangriff == "Papier") {
          enemy_damage(S_Faehigkeit1.F_Strength);
          attack_character_SF1_normal();
        }
      } 
      else if (S_Faehigkeit1.classification == "Stein") {
        if (Gegnerangriff == "Schere") {
          enemy_damage(S_Faehigkeit1.F_Strength);
          attack_character_SF1_normal();
        }
        if (Gegnerangriff == "Papier") {
        character_damage();
      } 
      }
      else if (S_Faehigkeit1.classification == "Papier") {
          if (Gegnerangriff == "Schere") {
            character_damage();
        } 
        else if (Gegnerangriff == "Stein") {
          enemy_damage(S_Faehigkeit1.F_Strength);
          attack_character_SF1_normal();
        }
      }
      S_Faehigkeit1.selected = false;
    } 


    else if (S_Faehigkeit2.selected == true) {
      if (S_Faehigkeit2.classification == Gegnerangriff) {
        info_tie()
      } 
      else if (S_Faehigkeit2.classification == "Schere") {
        if (Gegnerangriff == "Stein") {
          character_damage();
        } 
        else if (Gegnerangriff == "Papier") {
          enemy_damage(S_Faehigkeit2.K_Strength);
          attack_character_SF2_special();
        }
      } 
      else if (S_Faehigkeit2.classification == "Stein") {
        if (Gegnerangriff == "Schere") {
          enemy_damage(S_Faehigkeit2.K_Strength);
          attack_character_SF2_special();
        }
        else if (Gegnerangriff == "Papier") {
          character_damage();
        }
      } 
      else if (S_Faehigkeit2.classification == "Papier") {
        if (Gegnerangriff == "Schere") {
          character_damage();
        } 
        else if (Gegnerangriff == "Stein") {
          enemy_damage(S_Faehigkeit2.K_Strength);
          attack_character_SF2_special();
      }
    }
      S_Faehigkeit2.selected = false;
    } 


    else if (Faehigkeit.selected == true) {
      if (Faehigkeit.classification == Gegnerangriff) {
        info_tie()
      } 
      else if (Faehigkeit.classification == "Schere") {
        if (Gegnerangriff == "Stein") {
          character_damage();
        } 
        else if (Gegnerangriff == "Papier") {
          enemy_damage(Faehigkeit.F_Strength);
          attack_character_selected();
        }
      } 
      else if (Faehigkeit.classification == "Stein") {
        if (Gegnerangriff == "Schere") {
          enemy_damage(Faehigkeit.F_Strength);
          attack_character_selected();
        }
        if (Gegnerangriff == "Papier") {
          character_damage();
        }
      } 
      else if (Faehigkeit.classification == "Papier") {
        if (Gegnerangriff == "Schere") {
          character_damage();
        } 
        else if (Gegnerangriff == "Stein") {
          enemy_damage(Faehigkeit.F_Strength);
          attack_character_selected();       
        }
      }
      Faehigkeit.selected = false;
    }
  }
  else if (Spiele == false) {
      console.log("Das Spiel ist vorbei. Ich weiß nicht wieso du diese Nachricht überhaupt erhälst.");
  }
}

function playS_Faehigkeit1() { // Funktion wenn linker Button gedrückt wird
  button = 1; // 1 wird in Variable Button geschrieben, damit dieser später wieder sichtbar gemacht werden kann
  S_Faehigkeit1.selected = true; 
  document.getElementById("button1").style.backgroundColor = "#adafbc"; // Hintergrundfarbe des Buttons wird geändert, damit der Spieler ein visuelles Feedback hat, was gedrückt wurde
  label_pointer_off(); // Alle Buttons sind nicht mehr anklickbar
    gameLogic(); // Spiellogik wird ausgeführt
 }

function playS_Faehigkeit2() { // Funktion wenn mittlerer Button gedrückt wird, für Beschreibung siehe function playS_Faehigkeit1() 
  button = 2;
  S_Faehigkeit2.selected = true;
  document.getElementById("button2").style.backgroundColor = "#adafbc";
  label_pointer_off();
  gameLogic();
}

function play_Faehigkeit() { // Funktion wenn rechter Button gedrückt wird, für Beschreibung siehe function playS_Faehigkeit1() 
  button = 3;
  Faehigkeit.selected = true;
  document.getElementById("button3").style.backgroundColor = "#adafbc";
  label_pointer_off();
   gameLogic();
}

//Show hint window
$(document).ready(function () {
  $("#hint").modal("show"); // Hinweis-Modal-Winow wird angezeigt, sobald die Seite geladen wird
});

if ($("#hint").is(":visible")) {
  fadeInGame();
}

// Set-up the game with walking / spawning animations
// function idle() {
//   document.getElementById(`${character}_walk`).style.visibility = "hidden";
//   document.getElementById(`${character}_idle`).style.visibility = "visible";
// }

function setup() {
  document.getElementById(`${character}_idle`).style.visibility = "hidden"; // Idle-Animation für ausgewählten Charakter wird versteckt
  document.getElementById(`${character}_walk`).style.visibility = "visible"; // Lauf-Animation für ausgewählten Charakter wird angezeigt
  document.getElementById(`${character}_walk`).style.animation = "character-walk-in-r linear 6s"; // ausgewählter Charakter läuft nach rechts

  pauseFunction(6000, function () {
    document.getElementById(`${character}_walk`).style.visibility = "hidden"; // ausgewählter Charakter bleibt nach 6s stehen, da Lauf-Animation versteckt wird und Idle-Animation angezeigt wird
    document.getElementById(`${character}_idle`).style.visibility = "visible";
  });

  pauseFunction(7500, function () { // BloodMoonTower taucht auf und Hintergrundmusik wird abgespielt
    BloodMoonTower.classList.toggle("light");
    boss_theme.loop = true;
    boss_theme.play(); 
  });

  pauseFunction(9000, function () { // Charakter dreht sich um
    document.getElementById(`${character}_idle`).style.transform = "scale(-1, 1)"; 
  });

  pauseFunction(10000, function () { // Lauf-Animation taucht bei 500px von der linken Seite aus auf
    document.getElementById(`${character}_idle`).style.visibility = "hidden";
    document.getElementById(`${character}_walk`).style.left = "500px";
    document.getElementById(`${character}_walk`).style.transform = "scale(-1, 1)";
    document.getElementById(`${character}_walk`).style.visibility = "visible";
  });

  pauseFunction(10200, function () { // Charakter läuft wieder zurück
    document.getElementById(`${character}_walk`).style.animation = "character-walk-in-l linear 3s";
  });

  pauseFunction(13200, function () { // Charakter bleibt nach 3 Sekunden stehen und dreht sich um
    document.getElementById(`${character}_walk`).style.visibility = "hidden";
    document.getElementById(`${character}_idle`).style.transform = "scale(1, 1)";
    document.getElementById(`${character}_idle`).style.left = "50px";
    document.getElementById(`${character}_idle`).style.visibility = "visible";
  });

  pauseFunction(18000, function () { // Erster Gegner taucht auf
    Skeleton_idle.classList.toggle("light");
  });

  pauseFunction(19000, function () { 
    document.getElementById("options").style.visibility = "visible"; // Anzeige für Buttons tauchen auf
    document.getElementById("hearts-character-number").innerHTML = selectedKlasse.K_Leben; // Leben des Charakters werden an HTMl übergeben
    document.getElementById("hearts-enemy-number").innerHTML = Gegner[currentEnemy].G_Leben; // Leben des Gegners werden an HTMl übergeben

    document.getElementById("hearts-character").style.visibility = "visible"; // Anzeige der Leben des Charakters taucht auf
    document.getElementById("hearts-enemy").style.visibility = "visible"; // Anzeige der Leben des Gegners taucht auf
  });
}

// Fade-in and launch setup
function fadeInGame() {
  pauseFunction(1, function () {
    slideSourceIn.classList.toggle("light"); // Fade-In Animation für stage.html
  });
  setup(); // setup-Funktion wird ausgeführt
}

// Attack functions
function attack_character_selected() { // Funktion für die auswählbare Fähigkeit (rechter Button)
  if (selectedKlasse.K_Name == "Schwertkämpfer") { // Abfrage welche Charakter gespielt wird
    if (Faehigkeit.F_ID == 2) {
      attack_duration = 1200; // Dauer der Angriffsanimation in ms, da GIF sich immer wiederholt
    }
    if (Faehigkeit.F_ID == 3) {
      attack_duration = 2500; // siehe oben für ff.
    }
    if (Faehigkeit.F_ID == 4) {
      attack_duration = 1800; // siehe oben für ff.
    }
  }
  
  if (selectedKlasse.K_Name == "Priester") { // siehe oben für ff.
    if (Faehigkeit.F_ID == 7) {
      attack_duration = 1500; // siehe oben für ff.
    }
    if (Faehigkeit.F_ID == 8) {
      attack_duration = 800; // siehe oben für ff.
  
    }
    if (Faehigkeit.F_ID == 9) {
      attack_duration = 1400; // siehe oben für ff.
    }
  }
  
  if (selectedKlasse.K_Name == "Assassine") { // siehe oben für ff.
    if (Faehigkeit.F_ID == 12) {
      attack_duration = 600; // siehe oben für ff.
    }
    if (Faehigkeit.F_ID == 13) {
      attack_duration = 1500; // siehe oben für ff.
    }
    if (Faehigkeit.F_ID == 14) {
      attack_duration = 1800; // siehe oben für ff.
    }
  }
  document.getElementById(`${character}_idle`).style.visibility = "hidden"; // Idle-Animation wird versteckt
  document.getElementById(`${character}_${attack_selected}`).style.left = "50px"; // Angriffs-Animation des ausgewählten Charakters wird auf 50px von der linken Seite geschoben
  document.getElementById(`${character}_${attack_selected}`).style.visibility = "visible"; // Angriffs-Animation des ausgewählten Charakters wird sichtbar
  pauseFunction(`${attack_duration}`, function () { // Angriffs-Animation wird für einen Durchlauf abgespielt
    document.getElementById(`${character}_${attack_selected}`).style.visibility = "hidden"; // Angriffs-Animation des ausgewählten Charakters wird versteckt
    document.getElementById(`${character}_idle`).style.visibility = "visible"; // Idle-Animation wird wieder sichtbar
});
}

function attack_character_SF1_normal() { // Funktion für die normale Fähigkeit (linker Button), für Beschreibung siehe function attack_character_selected()
  if (selectedKlasse.K_Name == "Schwertkämpfer") {
      attack_duration = 1100;
  }
  if (selectedKlasse.K_Name == "Priester") {
      attack_duration = 700;
  }
  if (selectedKlasse.K_Name == "Assassine") {
      attack_duration = 800;
  }
  document.getElementById(`${character}_idle`).style.visibility = "hidden";
  document.getElementById(`${character}_${attack_SF1_normal}`).style.left = "50px";
  document.getElementById(`${character}_${attack_SF1_normal}`).style.visibility = "visible";
pauseFunction(`${attack_duration}`, function () {
  document.getElementById(`${character}_${attack_SF1_normal}`).style.visibility = "hidden";
  document.getElementById(`${character}_idle`).style.visibility = "visible";
});
}

function attack_character_SF2_special() { // Funktion für die Spezialfähigkeit (mittlerer Button), für Beschreibung siehe function attack_character_selected()
  if (selectedKlasse.K_Name == "Schwertkämpfer") {
      attack_duration = 1000;
  }
  if (selectedKlasse.K_Name == "Priester") {
      attack_duration = 3200;
  }
  if (selectedKlasse.K_Name == "Assassine") {
      attack_duration = 1100;
  }
  document.getElementById(`${character}_idle`).style.visibility = "hidden";
  document.getElementById(`${character}_${attack_SF2_special}`).style.left = "50px";
  document.getElementById(`${character}_${attack_SF2_special}`).style.visibility = "visible";
pauseFunction(`${attack_duration}`, function () {
  document.getElementById(`${character}_${attack_SF2_special}`).style.visibility = "hidden";
  document.getElementById(`${character}_idle`).style.visibility = "visible";
});
}

// Deathscreen and functions for replay / quit modal window
const deathScreen = document.getElementById("deathScreen");
function playDeathScreen() {
  $(boss_theme).animate({ volume: 0 }, 2000); // Fade-Out für Hintergrundmusik
  pauseFunction(2000, function () {
    document.getElementById("deathScreen").style.visibility = "visible"; // Deathscreen-Video wird sichtbar
    deathScreen.play(); // Deathscreen-Video wird abgespielt
  });
  pauseFunction(8000, function () {
    end.play(); // Nach 8 Sekunden wird die Game-Over-Musik abgespielt
    $("#gameover").modal("show"); // Game-Over Modal Window wird angezeigt
  });
}

function restart() {
  window.location.replace("./index.html"); // Wird im Game-Over Modal Window "Ja" gedrückt, wird zur index.html zurückgeleitet
}

function getOut() {
  window.location.replace("https://www.telekom.de/start"); // Wird im Game-Over Modal Window "Nein" gedrückt, wird zur besten Seite überhaupt weitergeleitet
}
