//### Backend JS ###//

let Logging = []; // Logging Klassen - Array
let LoggingSF = []; // Logging Standardfähigkeit - Array
let LoggingF = []; // Logging Fähigkeit - Array
let LoggingG = []; // Logging Gegner - Array

let color; // dient Zuordnung Farbe zu spezifischen Charakteren - String
let menu_screen, CharacterScreen, AbilityScreen; // Navigation - booleans

class Klassen { // Klassendeklaration für Klasse (Charakterwahl)
  constructor(K_ID, K_Name, K_Desc, K_Waffe, K_Leben, K_Strength,  K_selected = false) { // Construktor inkl. Instanzvariablen und K_selected ist standardmäßig false
    this.K_ID = K_ID; // Klassen - ID 
    this.K_Name = K_Name; // Klassen - Name
    this.K_Desc = K_Desc; // Klassen - Beschreibung
    this.K_Waffe = K_Waffe; // Klassen - Waffe 
    this.K_Leben = K_Leben; // Klassen - Leben 
    this.K_Strength = K_Strength; // Klassen Stärke = G_ID 
    this.K_selected = K_selected; // Boolean ob Klasse von Spieler gewählt wurde
  }

  getK_ID() {  //getter für K_ID 
    return this.K_ID;
  }

  getK_Name() {
    return this.K_Name;
  }

  getK_Desc() {
    return this.K_Desc;
  }

  getK_Strength() {
    return this.K_Strength;
  }

  getK_Waffe() {
    return this.K_Waffe;
  }

  getK_Leben() {
    return this.K_Leben;
  }

  getK_selected() {
    return this.K_selected;
  }
  setK_Selected(bool) { //setter für K_Selected, wir übergeben Bool
    this.K_selected = bool;
  }
}

class faehigkeit {// Klassendeklaration für Fähigkeit (Charakterwahl)
  constructor(F_ID, F_Name, F_Desc, F_Strength, FK_ID, F_selected = false) { // identisch zu Klasse Klasse
    this.F_ID = F_ID; // Fähigkeits - ID
    this.F_Name = F_Name; // Fähigkeits - Name
    this.F_Desc = F_Desc; // Fähigkeits - Beschreibung 
    this.F_Strength = F_Strength; // Fähigkeits - Stärke = G_ID 
    this.FK_ID = FK_ID; // Fähigkeits - Klassen - ID, Zuordnung Fähigkeit zu spezifischer Klasse 
    this.F_selected = F_selected; // Boolean ob Fähigkeit ausgewählt wurde oder nicht, Standard = False
  }

  getF_ID() { // getter für F_ID 
    return this.F_ID;
  }

  getF_Name() {
    return this.F_Name;
  }

  getF_Desc() {
    return this.F_Desc;
  }

  getF_Strength() {
    return this.F_Strength;
  }

  getFK_ID() {
    return this.FK_ID;
  }

  getF_selected() {
    return this.F_selected;
  }

  setF_selected(bool) { // Setter für F_Selected, wir übergeben einen Boolean bool
    this.F_selected = bool;
  }
}

class S_Faehigkeit extends faehigkeit { // S_Fähigkeit erbt alle Instanzvariablen von Faehigkeit und erweitert diese um F_Standard
  constructor(F_ID, F_Name, F_Desc, F_Strength, FK_ID, F_Standard, F_selected = false) { // Construktor für S_Faehigkeit = Standard_Faehigkeit
    super(F_ID, F_Name, F_Desc, F_Strength, FK_ID, F_selected); // hier definieren wir, was von der Elternklasse Faehigkeit geerbt werden soll, alle Variablen inkl. Methoden (getter und setter)
    this.F_Standard = F_Standard;
  }

  getF_Standard() {
    return this.F_Standard;
  }
}

class Gegner { // Klasse für Gegner, identisch zu Faehigkeiten und Klasse
  constructor(G_ID, G_Name, G_Desc, G_Leben) { // Construktor 
    this.G_ID = G_ID; // Gegner ID 
    this.G_Name = G_Name; // Gegner Name 
    this.G_Desc = G_Desc; // Gegner Beschreibung 
    this.G_Leben = G_Leben; // Gegner Leben
  }
  getG_ID() { // Getter für Gegner - ID
    return this.G_ID;
  }

  getG_Name() {
    return this.G_Name;
  }

  getG_Desc() {
    return this.G_Desc;
  }
  getG_Leben() {
    return this.G_Leben;
  }
}

async function loadKlasse() { // Funktion welche unsere Klassen von der Datenbank lädt, greift indirekt auf Backend zu // es handelt sich um eine asynchrone Funktion sodass wir await nutzen können
  try {
    const response = await fetch("http://localhost:8080/Klasse"); // wartet solange bis wir eine Rückmeldung vom Server erhalten, erst dann wird der Code weiter ausgeführt  
    const data = await response.json(); // das Gleiche nur dass wir hier warten, dass wir hier auf Klassen.json warten bis wir weiter machen können
    console.log(data); 

    for (let i = 0; i < data.length; i++) { // Schleife welche ganze Data welche wir erhalten durchläuft (Klasse.json)
      const item = data[i]; // json - Datein können via J-Script sehr einfach als Array ausgelesen werden. Jeder Datensatz ist ein Element des Arrays Data -> wir verwenden Item
      window[item.K_Name] = new Klassen( // wir erstellen eine neue Klasse, diese wird nach item.K_Name benannt, wir benötigen davor window da sonst die Klasse "item.K_Name" heißen würde..., window deklariert alle Variablen als global
        item.K_ID, item.K_Name, item.K_Desc, item.K_Waffe, item.K_Leben, item.K_Strength // wir lesen die jeweiligen Attribute der json aus und speichern diese in der Klasse, K_ID muss auch in der Datenbank K_ID genannt werden, sonst können wir darauf nicht zugreifen
      );
      Logging.push(item.K_Name); // Logging speichert den Namen, damit wir später darauf zugreifen können // push fügt die Daten am Ende des Arrays ein und erhöht die Länge um 1 
    }
  } catch (error) { // falls wir aus Gründen in einen Fehler laufen sollten, so wird dieser ausgegeben
    console.error("Fehler beim Laden der Daten: ", error);
  }
  return true;
}

async function loadStandardFaehigkeiten() { // System ist genau das selbe wie bei LoadKlassen bitte damit vergleichen 
  try {
    const response = await fetch('http://localhost:8080/Faehigkeiten');
    const data = await response.json();
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if (item.F_Standard == 1) { // da wir hier die Standardfähigkeiten eines Charakters laden wollen, muss bei der jeweiligen Fähigkeit F_Standard = 1 sein 
        window[item.F_Name] = new S_Faehigkeit(item.F_ID, item.F_Name, item.F_Desc, item.G_ID, item.K_ID, 1); // F_Standard wird auf 1 gesetzt, da sonst die IF-Bedingung zuvor nicht greifen würde
        LoggingSF.push(item.F_Name);
      }
    }
    return true;
  } catch (error) {
    console.log(error + " Es kam zu einem unterwarteten Fehler beim Auslesen der Standardfähigkeiten! ",  error);
  }

}

async function loadFaehigkeiten(id) { // System ist genau das selbe wie bei LoadKlassen, allerdings übergeben wir hier eine ID -> Klassen - ID 
  try {
    const response = await fetch("http://localhost:8080/Faehigkeiten"); // wir laden trotzdem alle Fähigkeiten
    const data = await response.json();
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if (item.K_ID == id && item.F_Standard == 0) { // und filtern hier die Daten, wir laden nur die Fähigkeiten, welche zu einem Charakter gehören. Wählt man z.B. Schwertkämpfer (K_ID = 0), so werden hier nur Fähigkeiten gespeichert, welche nicht Standard sind und die gleiche ID hinterlegt haben
        window[item.F_Name] = new faehigkeit(item.F_ID, item.F_Name, item.F_Desc, item.G_ID, id);
        LoggingF.push(item.F_Name);
      }
    }
  } catch (error) {
    console.error("Fehler beim Laden der Daten: ", error);
  }
  return true;
}

async function loadGegner() { // gleiche wie bei LoadKlasse() etc
  try {
    const response = await fetch("http://localhost:8080/Gegner");
    const data = await response.json();
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      window[item.G_Name] = new Gegner(item.G_ID, item.G_Name, item.G_Desc, item.G_Leben);
      LoggingG.push(item.G_Name);
    }
  } catch (error) {
    console.error("Fehler beim laden der Datein ", error);
    return false;
  }
  return true;
}

function loadData() { // fasst das Laden von loadKlasse(), loadGegner() und LoadStandardFaehigkeiten() in eine Funktion zusammen um die Navigation via Maus und Tastatur um Mainmenu zumindest bis zum ersten Modal-Window zu ermöglichen und Code-Dopplungen zu verhindern
  loadGegner().then(success => { // wir laden die Gegner, sofern von der funktion true returned wird, geben wir "Gegner wurden geladen! " aus, hierzu nutzen wir eine Callback Funktion
    if (success) {
      console.log("Gegner wurden geladen! ");
    }
  })
  loadStandardFaehigkeiten().then(success => { // ähnlich wie bei LoadGegner() 
    if (success) {
      console.log('Standardfähigkeiten wurden erfolgreich geladen!');
      document.getElementById('S_Faehigkeit1').textContent = window[LoggingSF[0]].getF_Name(); // hier ersetzen wir Text in der index.html mit Daten welche wir aus der Datenbank erhalten
      document.getElementById('S_Faehigkeit2').textContent = window[LoggingSF[1]].getF_Name(); // Es wird in der Index.html nach der ID "S_Faehigkeit2" gesucht

      document.getElementById('S_Faehigkeit1_1').textContent = window[LoggingSF[2]].getF_Name(); // wird dieser Verweis gefunden, so wird der TextContent zu entsprechenden Daten aus der
      document.getElementById('S_Faehigkeit1_2').textContent = window[LoggingSF[3]].getF_Name(); // Datenbank bzw jetzt von den Objekten geladen

      document.getElementById('S_Faehigkeit2_1').textContent = window[LoggingSF[4]].getF_Name();
      document.getElementById('S_Faehigkeit2_2').textContent = window[LoggingSF[5]].getF_Name();
    }
  })
  
  loadKlasse().then((success) => { // ähnlich wie bei LoadGegner()
    if (success) {
      console.log("Klassen wurden erfolgreich geladen, nun sollte die Ausgabe von Variablen möglich sein!");
      document.getElementById("character0").textContent = window[Logging[0]].getK_Name(); // ähnlich wie bei LoadStandardFaehigkeiten() 
      document.getElementById("K_Desc").textContent = window[Logging[0]].getK_Desc();
      document.getElementById("K_Waffe").textContent = window[Logging[0]].getK_Waffe();
      document.getElementById("K_Leben").textContent = window[Logging[0]].getK_Leben();
      document.getElementById("K_Strength").textContent = LoggingG[window[Logging[0]].getK_Strength()];

      document.getElementById("character1").textContent = window[Logging[1]].getK_Name();
      document.getElementById("K_Desc1").textContent = window[Logging[1]].getK_Desc();
      document.getElementById("K_Waffe1").textContent = window[Logging[1]].getK_Waffe();
      document.getElementById("K_Leben1").textContent = window[Logging[1]].getK_Leben();
      document.getElementById("K_Strength1").textContent = LoggingG[window[Logging[1]].getK_Strength()];

      document.getElementById("character2").textContent = window[Logging[2]].getK_Name();
      document.getElementById("K_Desc2").textContent = window[Logging[2]].getK_Desc();
      document.getElementById("K_Waffe2").textContent = window[Logging[2]].getK_Waffe();
      document.getElementById("K_Leben2").textContent = window[Logging[2]].getK_Leben();
      document.getElementById("K_Strength2").textContent = LoggingG[window[Logging[2]].getK_Strength()];
    }
  })
}

function readSelectedClass() { // Funktion zum Auslesen des Charakters, welcher vom Spieler gewählt wurde
  for (let i = 0; i < Logging.length; i++) { // Schleife durchläuft alle Einträge im Array Logging (Speichert Charaktere)
    if (window[Logging[i]].getK_selected() == true) { // prüft ob spezifischer Charakter von Logging[i] selected ist
      return window[Logging[i]].getK_ID(); // gibt ggf. dessen Namen zurück
    }
  }
}

function readSelectedAbility() { // Funktion zum Auslesen welche Fähigkeit gewählt wurde, ähnlich zu readSelectedClass()
  for (let i = 0; i < LoggingF.length; i++) {
    if (window[LoggingF[i]].getF_selected() == true) {
      console.log(i + LoggingF[i]);
      return i;
    }
  }
}

function setColor() { // Zuordnung Farbe zu ausgewähltem Charakter für das letzte Modal Window "openAbilities"
  characterName = Logging[readSelectedClass()] // Welcher Charakter wurde gewählt und welchen Namen hat dieser?
  if (characterName == "Schwertkämpfer") { // je nach Charakter wird eine Farbe für die Unterstreichung definiert, damit unser UI etwas individueller und ansprechender ist
    color = "chocolate";
    return color;
  }
  if (characterName == "Priester") {
    color = "dodgerblue";
    return color;
  }
  if (characterName == "Assassine") {
    color = "darkmagenta";
    return color;
  }
}

function saveallData() { // Funktion zum Übertragen von unseren Variablen damit wir diese in game.js nutzen können
  console.log("Ich speichere nun alle Daten!");
  K_ID = readSelectedClass(); // auslesen der vom Spieler gewählten Klassen - ID
  
  localStorage.setItem("LoggingG", JSON.stringify(LoggingG)); // hiermit speichern wir Logging G im LocalStorage, wir speichern diese als JSON - String damit wir Daten leicht auslesen können

  for (let i=0; i < LoggingG.length; i++) {
    localStorage.setItem(LoggingG[i], JSON.stringify(window[LoggingG[i]])); // wir speichern alle Gegner - Objekte
  }
  
  localStorage.setItem("Faehigkeit", JSON.stringify(window[LoggingF[readSelectedAbility()]])); // wir speichern die gewählte Fähigkeit im LocalStorage
  localStorage.setItem("selectedClass", JSON.stringify(window[Logging[K_ID]])); // Speichern des gewählten Charakter
  if (K_ID == 0) {
    localStorage.setItem("S_Faehigkeit1", JSON.stringify(window[LoggingSF[0]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
    localStorage.setItem("S_Faehigkeit2", JSON.stringify(window[LoggingSF[1]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
  } else if (K_ID == 1) {
    localStorage.setItem("S_Faehigkeit1", JSON.stringify(window[LoggingSF[2]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
    localStorage.setItem("S_Faehigkeit2", JSON.stringify(window[LoggingSF[3]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
  } else if (K_ID == 2) {
    localStorage.setItem("S_Faehigkeit1", JSON.stringify(window[LoggingSF[4]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
    localStorage.setItem("S_Faehigkeit2", JSON.stringify(window[LoggingSF[5]])); // Speichern der zu Charakter gehörenden Standard-Fähigkeiten
  }
}


function activateMenu_screen() {
  menu_screen = true;
  return true;
}

// Underline the first ability by default
function defaultAbilitySelector() {
  console.log("defaultAbilitySelector ausgeführt");
  document.getElementById("Faehigkeit_1").style.textDecoration = `underline ${color} 3px`; // der Name der ersten Fähigkeit von links wird unterstrichen
  document.getElementById("Faehigkeit_2").style.textDecoration = "none";
  document.getElementById("Faehigkeit_3").style.textDecoration = "none";
}

function pauseFunction(milliseconds, callback) { // Pause Funktion, pausiert Timer für bestimmte Zeit
  setTimeout(callback, milliseconds);
}

function fadeout() { // Fadeout zu stage.html und damit zum eigentlichen Spiel 
  slideSourceOut.classList.toggle('black');
  pauseFunction(3000, function() {
    window.location.replace("stage.html"); // Weiterleitung zu stage.html
  });
}




// ### Frontend JS ###//
// Sound variables
let select1 = new Audio("./media/sounds/select1.mp3");
let select2 = new Audio("./media/sounds/select2.mp3");
var theme = document.getElementById("theme")
let selected = new Audio("./media/sounds/selected.mp3");


// Show hint window
$(document).ready(function() { // Hinweis-Modal-Winow wird angezeigt, sobald die Seite geladen wird
  $('#hint').modal('show');
});

//Load and play background music in loop and fade-in
function menu(){
 theme.loop = true; // Hintergrundmusik wird in Schleife abgespielt
 theme.play(); // Hintergrundmusik startet
 slideSourceOut.classList.toggle('light'); // Fade-In des Hauptmenüs
}

function defaultCharakterSelector()  { // Funktion wird von Play Button ausgeführt, lädt alle Daten und gibt diese auf, öffnet Modal Window
  select1.play();
        document.getElementById("character0").style.textDecoration = "underline chocolate 3px"; // erstes Element unterstrichen, für Navigation relevant
        try {
          loadData(); // Gegner, Klassen, Standardfähigkeiten geladen
        } catch (error) {
          console.log("Ein fehler ist aufgetreten!");
        }
        $("#openCharacter").modal("show");
        menu_screen = false; // Menu - Navigation wird "deaktiviert"
        CharacterScreen = true; // Charakter - Screen Navigation wird "aktiviert"
  }

//Navigation in the main menu and the character selection
document.addEventListener("keydown", logHauptmenu);
menu_screen = true;
menu_position = 0;
character_position = 0;
ability_position = 0;

function logHauptmenu(e) {
  if (menu_screen == true) { // Abfrage ob man sich im Haptmenü befindet
    if (`${e.code}` == "ArrowLeft") { // Abfrage ob die linke Pfeiltaste gedrückt wird
      if (menu_position === 1 && (select1.paused || select1.currentTime == 0)) { // Abfrage, ob der rechte (credits) Button ausgewählt ist und der Auswahlsound nicht spielt
        $("#play").attr("src", "./media/images/mainmenu/play-selected.png"); // Play-Button wird ausgewählt
        $("#credits").attr("src", "./media/images/mainmenu/credits-unselected.png"); // Credits-Button abgewählt
        select1.play(); // Auswahlsound wird abgespielt
        menu_position = menu_position - 1; // Menü-Position wird 1 auf 0 gesetzt
      }
    }

    if (`${e.code}` == "ArrowRight") { // siehe oben für ff.
      if (menu_position === 0 && (select1.paused || select1.currentTime == 0)) {
        $("#play").attr("src", "./media/images/mainmenu/play-unselected.png");
        $("#credits").attr("src", "./media/images/mainmenu/credits-selected.png");
        select1.play();
        menu_position = menu_position + 1;
      }
    }

    if (`${e.code}` == "Enter" && !$('#hint').is(':visible') && (select1.paused || select1.currentTime == 0)) { // Abfrage ob die Entertaste gedrückt, das Hinweisfenster bereits geschlossen wurde und der Auswahlsound nicht spielt
      if (menu_position === 0 && (select1.paused || select1.currentTime == 0)) {
        select1.play(); // Auswahlsound wird abgespielt
        document.getElementById("character0").style.textDecoration = "underline chocolate 3px"; // erstes Element unterstrichen, für Navigation relevant
        try {
          loadData(); // Daten werden aus der Datenbank in die HTML-Seite geladen
        } catch (error) {
          console.log("Ein Fehler ist aufgetreten!");
        }
        $("#openCharacter").modal("show"); // Modal Window für Charakterauswahl wird angezeigt
        menu_screen = false;
        CharacterScreen = true;
      }
      if (menu_position == 1 && menu_screen == true && (select1.paused || select1.currentTime == 0)) { // Abfrage ob die Menü-Position 1 ist und der Auswahlsound nicht spielt
        select1.play(); // Auswahlsound wird abgespielt
        $("#openCredits").modal("show"); // Credits Modal Window wird angezeigt
        menu_screen = false;
      }
    }
  } else if (CharacterScreen == true) { // Abfrage ob man sich in der Charakterauswahl befindet
    if (`${e.code}` == "ArrowLeft") { // Abfrage ob die linke Pfeiltaste gedrückt wurde
      if (character_position == 1 && (select2.paused || select2.currentTime == 0)) { // Abfrage ob der mittlere Charakter ausgewählt wurde
        document.getElementById("character1").style.textDecoration = "none";
        document.getElementById("character0").style.textDecoration = "underline chocolate 3px"; // der Name des linken Charakters wird unterstrichen
        document.getElementById("character2").style.textDecoration = "none"; 
        select2.play(); // Auswahlsound wird abgespielt
        character_position = character_position - 1; // Charakterposition wird von 1 auf 0 gesetzt
      }
      if (character_position == 2 && (select2.paused || select2.currentTime == 0)) { 
        document.getElementById("character2").style.textDecoration = "none"; 
        document.getElementById("character1").style.textDecoration = "underline dodgerblue 3px"; // der Name des mittleren Charakters wird unterstrichen
        document.getElementById("character0").style.textDecoration = "none"; 
        select2.play(); // Auswahlsound wird abgespielt
        character_position = character_position - 1; // Charakterposition wird von 2 auf 1 gesetzt
      }
    }
    if (`${e.code}` == "ArrowRight") { // siehe oben für ff.
      if (character_position == 1 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("character1").style.textDecoration = "none";
        document.getElementById("character0").style.textDecoration = "none";
        document.getElementById("character2").style.textDecoration = "underline darkmagenta 3px";
        select2.play();
        character_position = character_position + 1;
      }
      if (character_position == 0 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("character0").style.textDecoration = "none";
        document.getElementById("character1").style.textDecoration = "underline dodgerblue 3px";
        document.getElementById("character2").style.textDecoration = "none";
        select2.play();
        character_position = character_position + 1;
      }
    }
    if (`${e.code}` == "Escape") { // Abfrage ob Escape-Taste gedrückt wurde
      console.log("There is no escape!");
      return false; // keine sichtbare Auswirkung
    } 

    if (`${e.code}` == "Enter" && (select1.paused || select1.currentTime == 0)) { // Abfrage ob Entertaste gedrückt wurde und der Auswahlsound nicht abgespielt wird
      if (character_position == 0) { // Abfrage, ob linker Charakter ausgewählt ist
        window[Logging[0]].setK_Selected(true);
        loadFaehigkeiten(0).then((success) => {
          if (success) {
            console.log("Fähigkeiten wurden erfolgreich geladen, nun sollten alle Fähigkeiten erreichbar sein");
            document.getElementById('Faehigkeit_1').textContent = window[LoggingF[0]].getF_Name();
            document.getElementById('F_Desc1').textContent = window[LoggingF[0]].getF_Desc();
            document.getElementById('F_Schwaeche1').textContent = LoggingG[window[LoggingF[0]].getF_Strength()];
            document.getElementById('Faehigkeit_2').textContent = window[LoggingF[1]].getF_Name();
            document.getElementById('F_Desc2').textContent = window[LoggingF[1]].getF_Desc();
            document.getElementById('F_Schwaeche2').textContent = LoggingG[window[LoggingF[1]].getF_Strength()];
            document.getElementById('Faehigkeit_3').textContent = window[LoggingF[2]].getF_Name();
            document.getElementById('F_Desc3').textContent = window[LoggingF[2]].getF_Desc();
            document.getElementById('F_Schwaeche3').textContent = LoggingG[window[LoggingF[2]].getF_Strength()];
          }
        })
        select1.play(); // Auswahlsound wird abgespielt
        setColor(); // Siehe Beschreibung der Funktion
        defaultAbilitySelector(); // Siehe Beschreibung der Funktion
        CharacterScreen = false; // auf false gesetzt, da man sich nun im Fähigkeitsauswahl-Modal-Window befindet
        AbilityScreen = true; // s.o.
        $("#faehigkeit1").attr("src", "./media/images/characters/schwertkaempfer/attack_2spin_endless.gif"); // Da Charakter 0 ausgewählt wurde, werden hier die auswählbaren Angriffe per GIF geladen
        $("#faehigkeit2").attr("src", "./media/images/characters/schwertkaempfer/attack_4jump_endless.gif");
        $("#faehigkeit3").attr("src", "./media/images/characters/schwertkaempfer/attack_3firehit_endless.gif");
        $('#openAbilities').modal('show');

      } else if (character_position == 1) { // siehe oben für ff.
        window[Logging[1]].setK_Selected(true);
        loadFaehigkeiten(1).then((success) => {
          if (success) {
            console.log("Fähigkeiten wurden erfolgreich geladen, nun sollten alle Fähigkeiten erreichbar sein");
            document.getElementById('Faehigkeit_1').textContent = window[LoggingF[0]].getF_Name();
            document.getElementById('F_Desc1').textContent = window[LoggingF[0]].getF_Desc();
            document.getElementById('F_Schwaeche1').textContent = LoggingG[window[LoggingF[0]].getF_Strength()];
            document.getElementById('Faehigkeit_2').textContent = window[LoggingF[1]].getF_Name();
            document.getElementById('F_Desc2').textContent = window[LoggingF[1]].getF_Desc();
            document.getElementById('F_Schwaeche2').textContent = LoggingG[window[LoggingF[1]].getF_Strength()];
            document.getElementById('Faehigkeit_3').textContent = window[LoggingF[2]].getF_Name();
            document.getElementById('F_Desc3').textContent = window[LoggingF[2]].getF_Desc();
            document.getElementById('F_Schwaeche3').textContent = LoggingG[window[LoggingF[2]].getF_Strength()];
          }
        })
        select1.play(); 
        setColor();
        defaultAbilitySelector();
        AbilityScreen = true;
        CharacterScreen = false;
        $("#faehigkeit1").attr("src", "./media/images/characters/priester/attack_2ice_stab.gif");
        $("#faehigkeit2").attr("src", "./media/images/characters/priester/attack_3power_stab.gif");
        $("#faehigkeit3").attr("src", "./media/images/characters/priester/attack_4splash.gif");
        $('#openAbilities').modal('show');

      } else if (character_position == 2) { // siehe oben für ff.
        window[Logging[2]].setK_Selected(true);
        loadFaehigkeiten(2).then((success) => {
          if (success) {
            console.log("Fähigkeiten wurden erfolgreich geladen, nun sollten alle Fähigkeiten erreichbar sein");
            document.getElementById('Faehigkeit_1').textContent = window[LoggingF[0]].getF_Name();
            document.getElementById('F_Desc1').textContent = window[LoggingF[0]].getF_Desc();
            document.getElementById('F_Schwaeche1').textContent = LoggingG[window[LoggingF[0]].getF_Strength()];
            document.getElementById('Faehigkeit_2').textContent = window[LoggingF[1]].getF_Name();
            document.getElementById('F_Desc2').textContent = window[LoggingF[1]].getF_Desc();
            document.getElementById('F_Schwaeche2').textContent = LoggingG[window[LoggingF[1]].getF_Strength()];
            document.getElementById('Faehigkeit_3').textContent = window[LoggingF[2]].getF_Name();
            document.getElementById('F_Desc3').textContent = window[LoggingF[2]].getF_Desc();
            document.getElementById('F_Schwaeche3').textContent = LoggingG[window[LoggingF[2]].getF_Strength()];
          }
        })
        select1.play();
        setColor();
        defaultAbilitySelector();
        AbilityScreen = true;
        CharacterScreen = false;
        $("#faehigkeit1").attr("src", "./media/images/characters/assassin/attack_2cut_twoway_endless.gif");
        $("#faehigkeit2").attr("src", "./media/images/characters/assassin/attack_4knifethrow_endless.gif");
        $("#faehigkeit3").attr("src", "./media/images/characters/assassin/attack_1fury_endless.gif");
        $('#openAbilities').modal('show');
      }
    }
  } else if (AbilityScreen == true) { // Abfrage ob man sich im Fähikeitenauswahlmenü befindet
    if (`${e.code}` == "ArrowLeft") { // Navigation mit den Pfeiltasten wie in den bisherigen Menüs, Unterstreichung der Fähigkeit ändert  sich mit Klick auf die Pfeiltasten
      if (ability_position == 1 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("Faehigkeit_2").style.textDecoration = "none";
        document.getElementById("Faehigkeit_3").style.textDecoration = "none";
        document.getElementById("Faehigkeit_1").style.textDecoration = `underline ${color} 3px`;
        select2.play();
        ability_position = ability_position - 1;
      }
      if (ability_position == 2 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("Faehigkeit_3").style.textDecoration = "none";
        document.getElementById("Faehigkeit_1").style.textDecoration = "none";
        document.getElementById("Faehigkeit_2").style.textDecoration = `underline ${color} 3px`;
        select2.play();
        ability_position = ability_position - 1;
      }
    }
    if (`${e.code}` == "ArrowRight") {
      if (ability_position == 1 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("Faehigkeit_2").style.textDecoration = "none";
        document.getElementById("Faehigkeit_1").style.textDecoration = "none";
        document.getElementById("Faehigkeit_3").style.textDecoration = `underline ${color} 3px`;
        select2.play();
        ability_position = ability_position + 1;
      }
      if (ability_position == 0 && (select2.paused || select2.currentTime == 0)) {
        document.getElementById("Faehigkeit_1").style.textDecoration = "none";
        document.getElementById("Faehigkeit_3").style.textDecoration = "none";
        document.getElementById("Faehigkeit_2").style.textDecoration = `underline ${color} 3px`;
        select2.play();
        ability_position = ability_position + 1;
      }
    }

    if ((`${e.code}` == "Enter") && (select1.paused || select1.currentTime == 0)) { // Abfrage, ob im Fähikeitenauswahlmenü die Enter-Taste gedrückt wurde und der Auswahlsound nicht spielt
      if (ability_position == 0) { // Abfrage, ob die linke Fähigkeit ausgewählt wurde
        console.log("Bedingung ist true!");
        window[LoggingF[0]].setF_selected(true);
        console.log(window[LoggingF[0]].getF_Name());
        saveallData(); 
        document.getElementById("slideSourceOut").style.opacity = "0"; // Fade-Out des sichtbaren Bildschirms
        document.getElementById("slideSourceOut").style.transition = "opacity 4s"; 
        fadeout();
        selected.play(); // Auswahlsound wird abgespielt
        $(theme).animate({ volume: 0 }, 2000); // Fade-Out für Hintergrundmusik 
      }

      if (ability_position == 1) { // siehe oben für ff.
        console.log("Bedingung ist true!");
        window[LoggingF[1]].setF_selected(true);
        console.log(window[LoggingF[1]].getF_Name());
        saveallData();
        document.getElementById("slideSourceOut").style.opacity = "0";
        document.getElementById("slideSourceOut").style.transition = "opacity 4s";
        fadeout();
        selected.play();
        $(theme).animate({ volume: 0 }, 2000);
       }

      if (ability_position == 2) { // siehe oben für ff.
        console.log("Bedingung ist true!");
        window[LoggingF[2]].setF_selected(true);
        console.log(window[LoggingF[2]].getF_Name());
        saveallData();
        document.getElementById("slideSourceOut").style.opacity = "0";
        document.getElementById("slideSourceOut").style.transition = "opacity 4s";
        fadeout();
        selected.play();
        $(theme).animate({ volume: 0 }, 2000); 
      }
    }
  }
}