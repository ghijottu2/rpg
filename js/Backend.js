import express from "express"; // Express-Erweiterung
import mysql from "mysql2"; // MySQL - Erweiterung
import cors from "cors"; // Cors - Erweiterung


const app = express(); // Initialisierung unserer Express - Anwendung 
app.use(cors()); // Cors verwaltet zulässige Anfragen an unseren Express Server. Müssen wir nicht genauer definieren, da nur lokale Anfragen gesendet werden. 
app.use(express.json()); // unsere Express - Anwendung unterstützt json - Formate
app.use(express.urlencoded({ extended: false })); // hierbei definieren wir, dass wir die Bibliothek queryString zum parsen der Daten nutzen (simpler als qs und ausreichend)

const connection = mysql.createConnection({ // Aufbau einer Verbindung inkl. angegebenen Daten wenn connection aufgerufen wird
  host: "127.0.0.1",
  user: "Database", // MySQL-Benutzername
  password: "Telekom123", // MySQL-Passwort
  database: "projekt", // Name MySQL-Datenbank
});

connection.connect((err) => { // Herstellen Verbindung zur Datenbank 
  if (err) {
    console.error("Fehler bei der Verbindung" + err.stack);  // erhalten wir einen Fehler, so wird dieser ausgegeben
    return;
  }
  console.log("Erfolgreich verbunden, congrats!"); // ohne Fehler ist die Verbindung erfolgreich 
});

app.get("/Klasse", (request, response) => { // diese Funktion wird aufgerufen, wenn wir http://localhost:8080/Klasse aufrufen bzw. fetchen wollen
  connection.query("select * from klassen;", (err, rows) => { // stellen eine Anfrage an unsere Datenbank und erhalten entweder Fehlermeldung als "err" oder die Daten als "rows"
    if (err) {
      response.status(500).send("Fehler beim Laden der Daten"); // bei einem Fehler beenden wir die Funktion und geben Fehler 500 (internal server error) aus
      return;
    }

    response.json(rows); // erhalten wir eine Antwort, so erstellen wir daraus eine json-Datei welche das Frontend erreichen kann
  });
});

app.get("/Faehigkeiten", (request, response) => { // genau das gleiche wie bei /Klasse -> bitte hier nachlesen
  connection.query("select * from faehigkeiten;", (err, rows) => {
    if (err) {
      response.status(500).send("Fehler beim Laden der Daten");
      return;
    }
    response.json(rows);
  });
});

app.get("/Gegner", (request, response) => { // genau das gleiche wie bei /Klasse -> bitte hier nachlesen
  connection.query("select * from gegner;", (err, rows) => {
    if (err) {
      response.status(500).send("Fehler beim Laden der Daten");
      return;
    }
    response.json(rows);
  });
});

app.listen(8080, () => { // unser Server wird auf Port 8080 initialisiert und kann auf diesem Port angesprochen werden, Port ist variabel!
  console.log("Erfolgreich den Server gestartet!"); // RÜckmeldung dass der Server gestartet wurde. Wir fangen keine Fehler ab, da wir bisher keine hatten. :) 
});
